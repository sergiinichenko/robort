# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 14:15:27 2020

@author: nichenko_s
"""
#%%
from robot.imports import *

class Logic(LogicBase):
    
    def setup(self):

        """ setting trapezoidal fuzzy logic rules """
        Close = Fuzzy(0, 0, .13, .18)
        Normal = Fuzzy(.13, .18, .20, .25)
        Far = Fuzzy(.20, .25, 8000, 10000)
        
        dist = Set()
        dist.set.append(Close)
        dist.set.append(Normal)
        dist.set.append(Far)
        
        speed = Set()
        speed.set.append(Fuzzy(0., 0.05, 0.1, 0.2))
        speed.set.append(Fuzzy(0.1, 0.2, 0.2, 0.3)) 
        speed.set.append(Fuzzy(0.2, 0.3, 0.3, 0.4))
        
        angle = Set()
        angle.set.append(Fuzzy(1, 1.5, 1.5, 2.6))
        angle.set.append(Fuzzy(0, 0.5, 0.9, 1.3)) 
        angle.set.append(Fuzzy(-0.5, 0, 0, 0.5))
        
        self.rule_speed = Rules(dist, speed)
        self.rule_angle = Rules(dist, angle)


    def getA(self, memory):
        """ a -- derives a sign (+ or -1) which is translated 
        to the direction of the turn """

        a = memory.RL - memory.RR
        if a == 0.0:
            a = 1
        else:
            a = a/np.abs(a)
        return a


    def calculate(self, memory): #x, y, dx, dy, objects):
        
        """ main function. It is being called on every time step by
        the robot itself to determine the response of the logic to 
        the input parameters """
        
        """ defines the direction (in radians relative to the current 
        direction of the robot) based on the memory of the robot about 
        the environment (objects and walls) and past trajectory """
        corr = memory.getDirection() * 1.

        """ determination of sought-for speed and angle """
        if memory.state == State.stuck:
            return 0.05 * self.rule_speed.output(memory.R), self.rule_angle.output(memory.R)
        
        """ logic for the normal operation of the robot. getA returns the direction 
        of the turn (-1 right, 1 left) """
        if memory.state == State.normal:
            return self.rule_speed.output(memory.R), self.getA(memory) * self.rule_angle.output(memory.R) + corr


#%%
"""importing a picture of room"""
room  = Room("rooms/room7.png")
logic = Logic()

"""setting needed time of simulation"""
sys = System(time = 60. * 30.0) 
sys.robot.setupLogic(logic)
sys.setRoom(room)
sys.run()


#%%
"""start animation"""
anim = Animation(sys.robot, sys.room)
anim.animate()
