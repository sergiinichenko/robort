import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.widgets import Button, RadioButtons, Slider
COLORS = np.array(['#FCFFEC', '#B4D2F1', '#85C1E9', '#2E44AB', '#196F3D', '#CB4335', '#58D68D', '#2874A6', 
          '#A2D9CE', '#935116', '#DC7633', '#E59866', '#154360', '#16A085', '#7D6608', '#313131'])

class Animation:
    def __init__(self, robot, room):
        self.robot = robot
        self.room  = room
        self.buff_x = []
        self.buff_y = []
        self.display_len = len(self.robot.memory.x)

        # set up the plot
        self.fig = plt.figure(figsize=(12,12))
        self.ax = self.fig.add_subplot(111, xlim=(0, self.room.x.max()), ylim=(0.0, self.room.y.max()))
        #self.path    = self.ax.scatter([], [], linewidth=20, alpha=0.2)
        x, y, z = self.robot.memory.getMapXY()
        self.map     = self.ax.scatter(x, y, alpha=0.3, s=50, marker="s", c=COLORS[z.astype(int)].flatten())
        self.path    = self.ax.plot([], [], linewidth=25, alpha=0.25, color='#3C8E83')
        self.robort  = self.ax.scatter([], [], s=500, color='#14C1AA')
        #self.room    = self.ax.scatter(self.room.x, self.room.y, color='#696969', alpha=0.2)
        # setup the animation
        self.cur_frame = 0
        self.step = 1.0

    def _add_to_buff(self, buf, val):
        if len(buf) < self.display_len:
            buf.append(val)
        else:
            #buf.popleft()
            buf.append(val)

    def _update(self, frame):
        frame = self.cur_frame
        self._add_to_buff(self.buff_x, self.robot.memory.x[frame])
        self._add_to_buff(self.buff_y, self.robot.memory.y[frame])

        #self.path.set_offsets(np.c_[self.buff_x, self.buff_y])
        self.path[0].set_data(self.buff_x, self.buff_y)
        self.cur_frame += 1
        self.slider.set_val(self.cur_frame)

        self.robort.set_offsets(np.c_[self.buff_x[frame], self.buff_y[frame]])
        return self.path

    def _pause(self, event):
        if self.anim_running:
            self.anim.event_source.stop()
            self.anim_running = False
        else:
            self.anim.event_source.start()
            self.anim_running = True

    def _reset(self, event):
        self.buff_x = []
        self.buff_y = []
        self.cur_frame = 0
        self.ax.clear()
        #self.path    = self.ax.scatter([], [], linewidth=20, alpha=0.2)
        # set up the plot
        self.ax.set_xlim((0.0, self.room.x.max()))
        self.ax.set_ylim((0.0, self.room.y.max()))
        x, y, z = self.robot.memory.getMapXY()
        self.map     = self.ax.scatter(x, y, alpha=0.3, s=50, marker="s", c=COLORS[z.astype(int)].flatten())
        self.path    = self.ax.plot([], [], linewidth=25, alpha=0.25, color='#3C8E83')
        self.robort  = self.ax.scatter([], [], s=500, color='#14C1AA')
        #self.room    = self.ax.scatter(self.room.x, self.room.y, color='#7B241C')
        # setup the animation
        self.cur_frame = 0
        self.step = 1.0

    def _left(self, event):
        self.buff_x.pop()
        self.buff_y.pop()
        self.cur_frame -= 1
        #self.path.set_offsets(np.c_[self.buff_x, self.buff_y])
        self.path[0].set_data(self.buff_x, self.buff_y)
        self.robort.set_offsets(np.c_[self.buff_x[self.cur_frame-1], self.buff_y[self.cur_frame-1]])
        self.slider.set_val(self.cur_frame)
        self.fig.canvas.draw()

    def _left10(self, event):
        self.buff_x = self.buff_x[:-10]
        self.buff_y = self.buff_y[:-10]
        self.cur_frame -= 10
        #self.path.set_offsets(np.c_[self.buff_x, self.buff_y])
        self.path[0].set_data(self.buff_x, self.buff_y)
        self.robort.set_offsets(np.c_[self.buff_x[self.cur_frame-1], self.buff_y[self.cur_frame-1]])
        self.slider.set_val(self.cur_frame)
        self.fig.canvas.draw()

    def _left100(self, event):
        self.buff_x = self.buff_x[:-100]
        self.buff_y = self.buff_y[:-100]
        self.cur_frame -= 100
        #self.path.set_offsets(np.c_[self.buff_x, self.buff_y])
        self.path[0].set_data(self.buff_x, self.buff_y)
        self.robort.set_offsets(np.c_[self.buff_x[self.cur_frame-1], self.buff_y[self.cur_frame-1]])
        self.slider.set_val(self.cur_frame)
        self.fig.canvas.draw()

    def _right(self, event):
        self.buff_x.append(self.robot.memory.x[self.cur_frame])
        self.buff_y.append(self.robot.memory.y[self.cur_frame])
        
        #self.path.set_offsets(np.c_[self.buff_x, self.buff_y])
        self.path[0].set_data(self.buff_x, self.buff_y)
        self.robort.set_offsets(np.c_[self.buff_x[self.cur_frame], self.buff_y[self.cur_frame]])
        self.cur_frame += 1
        self.slider.set_val(self.cur_frame)
        self.fig.canvas.draw()

    def _right10(self, event):
        self.buff_x = self.buff_x + self.robot.memory.x[self.cur_frame:self.cur_frame+10]
        self.buff_y = self.buff_y + self.robot.memory.y[self.cur_frame:self.cur_frame+10]
        self.path[0].set_data(self.buff_x, self.buff_y)

        self.cur_frame += 10
        self.robort.set_offsets(np.c_[self.buff_x[self.cur_frame-1], self.buff_y[self.cur_frame-1]])

        self.slider.set_val(self.cur_frame)
        self.fig.canvas.draw()

    def _right100(self, event):
        self.buff_x = self.buff_x + self.robot.memory.x[self.cur_frame:self.cur_frame+100]
        self.buff_y = self.buff_y + self.robot.memory.y[self.cur_frame:self.cur_frame+100]
        self.path[0].set_data(self.buff_x, self.buff_y)

        self.cur_frame += 100
        self.robort.set_offsets(np.c_[self.buff_x[self.cur_frame-1], self.buff_y[self.cur_frame-1]])

        self.slider.set_val(self.cur_frame)
        self.fig.canvas.draw()

    def _set_pos(self, event):
        self.cur_frame = int(self.slider.val)
        self.buff_x = self.robot.memory.x[:self.cur_frame]
        self.buff_y = self.robot.memory.y[:self.cur_frame]
        #self.path.set_offsets(np.c_[self.buff_x, self.buff_y])
        self.path[0].set_data(self.buff_x, self.buff_y)
        self.robort.set_offsets(np.c_[self.buff_x[self.cur_frame-1], self.buff_y[self.cur_frame-1]])
        self.fig.canvas.draw()

    def animate(self):
        left_ax = self.fig.add_axes((0.1, 0.025, 0.1, 0.04))
        self.left_100 = Button(left_ax, '-100', hovercolor='0.975')
        self.left_100.on_clicked(self._left100)

        left_ax = self.fig.add_axes((0.2, 0.025, 0.1, 0.04))
        self.left_10 = Button(left_ax, '-10', hovercolor='0.975')
        self.left_10.on_clicked(self._left10)

        left_ax = self.fig.add_axes((0.3, 0.025, 0.1, 0.04))
        self.left_button = Button(left_ax, 'Left', hovercolor='0.975')
        self.left_button.on_clicked(self._left)

        pause_ax = self.fig.add_axes((0.4, 0.025, 0.1, 0.04))
        self.pause_button = Button(pause_ax, 'Play', hovercolor='0.975')
        self.pause_button.on_clicked(self._pause)

        reset_ax = self.fig.add_axes((0.5, 0.025, 0.1, 0.04))
        self.reset_button = Button(reset_ax, 'Reset', hovercolor='0.975')
        self.reset_button.on_clicked(self._reset)

        right_ax = self.fig.add_axes((0.6, 0.025, 0.1, 0.04))
        self.right_button = Button(right_ax, 'Right', hovercolor='0.975')
        self.right_button.on_clicked(self._right)

        right_ax = self.fig.add_axes((0.7, 0.025, 0.1, 0.04))
        self.right_10 = Button(right_ax, '+10', hovercolor='0.975')
        self.right_10.on_clicked(self._right10)

        right_ax = self.fig.add_axes((0.8, 0.025, 0.1, 0.04))
        self.right_100 = Button(right_ax, '+100', hovercolor='0.975')
        self.right_100.on_clicked(self._right100)

        sliderax = self.fig.add_axes((0.12, 0.07, 0.78, 0.02))
        self.slider = Slider(sliderax, '', 0.0, len(self.robot.memory.x), valinit=self.cur_frame)
        self.slider.on_changed(self._set_pos)

        self.anim = animation.FuncAnimation(self.fig, self._update,
                                            interval=self.step)


        # setup the animation control
        self.anim_running = True
        plt.show()

