# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 09:36:28 2020

@author: nichenko_s
"""
import numpy as np


class LogicBase:
    def __init__(self, teta=80):
         self.x = 0.0
         self.y = 0.0
         self.teta = teta
         self.cos  = np.cos(self.teta / 180.0 * np.pi)
         self.sin  = np.sin(self.teta / 180.0 * np.pi)
         self.rules = []
         self.setup()
         
    def setup(self):
        pass
    
    def calculate(self, info): #x, y, dx, dy, objects):
        """
        teta = info.teta
        dd   = info.dd
        if info.x >= 5.0 or info.x < 0.0:
             teta = -info.teta + (0.002*np.random.rand()-0.001)
        if info.y >= 5.0 or info.y < 0.0:
             dy = -info.dy + (0.005*np.random.rand()-0.0025)
        
        if info.R <= 0.2:
             dx = info.dx * self.cos + info.dy * self.sin
             dy = info.dx * (-self.sin) + info.dy * self.cos
        if info.R <= 0.2:
             teta = info.teta - self.teta / 180.0 * np.pi
            
        return dd, teta
        """
        pass
