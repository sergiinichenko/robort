# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 14:15:27 2020

@author: nichenko_s
"""
#%%

import matplotlib.pyplot as plt
import numpy as np

from robot.animation import Animation
from robot.robot import Robot, State
from robot.controls import LogicBase
from robot.room import Room
from robot.fuzzy import Fuzzy, Set, Rules
import robot.utils as ut

class System:
    def __init__(self, dtau = 0.1, time=1000): # time = default time
        self.dtau = dtau
        self.tau  = 0.0
        self.robot = Robot(self.dtau)
        self.logic = LogicBase()
        self.x = None
        self.y = None
        self.room = None
        self.time = time

    def run(self):
        self.x = []
        self.y = []
        self.x.append(self.robot.x)
        self.y.append(self.robot.y)         
        for i in range(0, int(self.time/self.dtau)):
            self.robot.detectObjects(self.room)
            self.robot.defineState()
            self.robot.Analyze()
            self.robot.move()

    def setRoom(self, room):
        self.room = room
        self.room.y = self.room.y.max() - self.room.y
