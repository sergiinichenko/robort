from robot.animation import Animation
from robot.robot import Robot, State
from robot.controls import LogicBase
from robot.room import Room
from robot.fuzzy import Fuzzy, Set, Rules
from robot.system import System
import numpy as np