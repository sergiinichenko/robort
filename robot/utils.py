# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 09:42:09 2020

@author: nichenko_s
"""

import numpy as np
pi2 = np.pi * 2.0

def angle(dx, dy):

    """ calculated the angle in radian of the (dx, dy) vector
    relative to the x axis """
    
    r = (dx*dx + dy*dy)**0.5
    if r == 0.0:
        return 0.0
    else:
        ang = np.arccos(dx/r)
        ang = pi2-ang if dy < 0.0 else ang
        return ang

def smooth(input):
    
    """ Performes singular smoothing of the matrix by averaging 
    each element by itself and its closest neighbours """
    
    a = np.zeros(input.shape)
    b = np.zeros(input.shape)
    a[:,1:-1] = (input[:,2:] + input[:,1:-1] + input[:,:-2])*0.3333
    a[:,0]    = (input[:,0]  + input[:,1])*0.5
    a[:,-1]   = (input[:,-1] + input[:,-2])*0.5
    b[1:-1,:] = (a[2:,:] + a[1:-1,:] + a[:-2,:])*0.3333
    b[0,:]    = (a[0,:]  + a[1,:])*0.5
    b[-1,:]   = (a[-1,:] + a[-2,:])*0.5
    return b


def angleDiff( a1, a2 ):

    """ return the shortest angular difference between two
    angles """

    diff = ( a2 - a1 + np.pi ) % pi2 - np.pi
    res = diff + pi2 if diff < -np.pi else diff
    return res


def getWeightned(input, dx_map):
    
    """ returns the averaged position of the 0s in the 
    input (surrounding of the robot from the memory map) """

    xc = input.shape[1]*dx_map * 0.5 - 0.5*dx_map
    yc = input.shape[0]*dx_map * 0.5 - 0.5*dx_map
    w = input.copy()

    w[w==0.0] = 0.2
    w = 1.0 / w
    xx = np.arange(0, input.shape[1]) * dx_map
    yy = np.arange(0, input.shape[0]) * dx_map
    xx, yy = np.meshgrid(xx, yy)
    xw = np.sum(xx*w)/np.sum(w)
    yw = np.sum(yy*w)/np.sum(w)
    dx =  xw - xc
    dy =  yw - yc
    if np.abs(dx) < 0.01 and np.abs(dy) < 0.01:
        dx = 0.0
        dy = 0.0
    return dx, dy