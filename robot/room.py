# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 10:28:24 2020

@author: nichenko_s
"""
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

class Room:
    def __init__(self, im = None, xlim=5.0, ylim=5.0):
        self.x = []
        self.y = []
        if im is not None:
            self.setup(im, xlim, ylim)
        
    def setup(self, path, xlim=5.0, ylim=5.0):
        im = Image.open(path)
        data = np.array(im)[:,:,:-1].sum(axis=2)#[:,:,0], np.add(np.array(im)[:,:,1], np.array(im)[:,:,2]))
        
        # Create the grid of a size of the supplied image
        nx, ny = np.array(im).shape[0:2]
        x = np.linspace(0., xlim, nx)
        y = np.linspace(0., ylim, ny)
        x, y = np.meshgrid(x, y)
        
        # Remove the x, y elements where data is below 750
        self.x = x[data<=750]
        self.y = y[data<=750]
        
    def show(self):
        fig = plt.figure(figsize=(10.0,8.0))
        ax = fig.add_axes([0.0,0.0,1.0,1.0])
        ax.scatter(self.x, self.y, marker="o")
        