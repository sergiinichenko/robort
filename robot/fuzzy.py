# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 10:42:35 2020

@author: nichenko_s
"""
class Fuzzy: 
    def __init__(self, x1, x2, x3, x4):
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
        self.x4 = x4
        
    def get_y (self, x):
        y = 0.0
        if x < self.x2 and x > self.x1:
            y = (1.0/(self.x2 - self.x1))*(x - self.x1)
        if x >= self.x2 and x <= self.x3:
            y = 1.0
        if x > self.x3 and x < self.x4:
            y = 1 - (1.0/(self.x4 - self.x3))*(x - self.x3)
        return y
    
    def get_x (self, y=1.0):
       x_2 = self.x1 + y * (self.x2 - self.x1)
       x_3 = self.x4 - y * (self.x4 - self.x3)
       a = x_3 - x_2
       b = self.x4 - self.x1
       c = x_2 - self.x1
       x = self.x1 + (2 * a * c + a**2 + c * b + a * b + b**2) / (3 * (a + b))
       return x
   
    def area (self, y=1.0):
       x_2 = self.x1 + y * (self.x2 - self.x1)
       x_3 = self.x4 - y * (self.x4 - self.x3)
       a = x_3 - x_2
       b = self.x4 - self.x1
       return y * (a + b)/2.
   
class Set:
    def __init__(self):
        self.set = []
        
    def get_y (self, x):
        ys = []
        
        for s in self.set:
            ys.append(s.get_y(x))
        return ys

    def get_x(self, ys):
        sum1 = 0.0
        sum2 = 0.0
        for s, y in zip(self.set, ys):
            sum1 = sum1 + s.area(y) * s.get_x(y)
            sum2 = sum2 + s.area(y)
        return sum1/sum2
    
class Rules:
    def __init__(self, input, output):
        self.i = input
        self.o = output
    
    def output(self, signal):
        return self.o.get_x(self.i.get_y(signal))
