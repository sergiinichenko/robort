# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 09:34:21 2020

@author: nichenko_s
"""
import numpy as np
from robot.sensor import Sensor, AngleSensor, SensorData
import scipy as sp
import robot.utils as ut

# 360 degrees in radian
pi2 = np.pi * 2.0

# this one is used in map coverage routines
rs = [[0.0, 0.0], [0.1, 0.0], [0.75, 0.75], [0.0, 1.0], 
     [-0.75, 0.75], [-1.0, 0.0], [-0.75, -0.75], [0.0, -0.1], [-0.75, 0.75]]

# enum for the robots state
class State:
    normal = 0
    stuck = 1

class Memory:
    def __init__(self):
        # current x and y
        self.xc = 0.0
        self.yc = 0.0
        self.dr = 0.0
        self.dl = 0.0
        self.R  = 0.1
        self.RL = 0.0
        self.RR = 0.0
        self.RD = 0.0
        self.Lp = 0.0
        self.Rp = 0.0
        self.b  = 0.1
        self.teta  = 0.0
        self.dd    = 0.0
        self.dteta = 0.0
        self.alphs = 0.0
        self.x = []
        self.y = []
        self.tau = 0.0
        self.state = State.normal
        self.map = np.zeros((2,2))

        # resolution of the map: 5cm x 5cm blocks
        self.dx_map = 0.05
        self.potential = np.zeros((1,1))
        
    def copy(self, other):
        """ Copies the data from the robot to its memory """
        self.teta = other.teta
        self.dd = other.dd
        self.dteta = other.dteta
        self.xc = other.x# + other.dtau * self.dd * np.cos(self.teta + other.dtau * self.dteta * 0.5)
        self.yc = other.y# + other.dtau * self.dd * np.sin(self.teta + other.dtau * self.dteta * 0.5)
        
    def addXY(self, x, y):
        """ Appends the current x, y coordinates to the memory """
        self.x.append(x)
        self.y.append(y)
        

    def setState(self, state):
        """ Sets the state of the memory"""
        self.state = state
        self.tau = 0.0

        
    def updateMap(self, other):
        """  Updated and expands the memory map around the robot.
        If needed expands the map. If it sees the wall it
        defines the wall blocks in the memory """

        # rs contain 7 x, y pairs for the current block and 6 blocks around it.
        # This is needed to expand the robots coverage for the blocks it "cleans"
        self.expandMap(other.x + 1.0, other.x + 1.0)
        for r in rs:
            x = other.x + self.dx_map*r[0]
            y = other.y + self.dx_map*r[1]
            self.expandMap(x, x)
            self.setMapValue(x, y, 1.0)
        self.defineWall()
        self.definePot()
        print(np.sum(self.map==1) / (np.sum(self.map==1) + np.sum(self.map==0)))

    def expandMap(self, x, y):
        """ This code expands memory map to include the x, y block if 
        it is not in the memory yet """

        # translates the x, y coordinates to the indeces of the map matrix
        ix = int( x / self.dx_map)+1
        iy = int( y / self.dx_map)+1

        # expands the x, y matrix in the corresponding direction
        # and fills the new blocks with 0-s
        if self.map.shape[1] < ix:
            self.map = np.append(self.map, 
                                np.zeros((self.map.shape[0], ix - self.map.shape[1])), 
                                axis=1)

        if self.map.shape[0] < iy:
            self.map = np.append(self.map, 
                                np.zeros((iy - self.map.shape[0], self.map.shape[1])), 
                                axis=0)
    
    
    def setMapValue(self, x, y, val):
        """ sets the memory value at the x, y coordinates to val """

        ix = int( x / self.dx_map)
        iy = int( y / self.dx_map)
        self.map[iy, ix] = val


    def getMapXY(self):
        """ Return ths x, y and z data from the map. The z data
        corresponds to the value in the x, y map matrix """

        x = np.linspace(0, self.map.shape[1]*self.dx_map, self.map.shape[1])
        y = np.linspace(0, self.map.shape[0]*self.dx_map, self.map.shape[0])
        x, y = np.meshgrid(x, y)
        return x, y, self.map

    def getPotentialXY(self):
        """ Return ths x, y and z data from the map. The z data
        corresponds to the value in the x, y map matrix """

        x = np.linspace(0, self.potential.shape[1]*self.dx_map, self.potential.shape[1])
        y = np.linspace(0, self.potential.shape[0]*self.dx_map, self.potential.shape[0])
        x, y = np.meshgrid(x, y)
        return x, y, self.potential


    def defineWall(self):
        """ This routine analyzes data from 3 sensors (front and two side sensors).
        Knowing the angles and distances from every sensor it can define, 
        where the obstacle block is located. Then it marks the corresponding
        block in the memory by setting its value to 2 """

        if self.RD < 0.40:
            # defines the coordinaes of the obstacle from the distance and angle
            # for the direct (front) sensor
            x = self.xc + np.cos(self.teta) * self.RD
            y = self.yc + np.sin(self.teta) * self.RD
            
            # expands the memory if needed
            self.expandMap(x, y) 

            # sets the memory value to 2 (for obstacle)
            self.setMapValue(x, y, 3.0)

        if self.Rp < 0.40:
            # defines the coordinaes of the obstacle from the distance and angle
            # for the right precise sensor
            x = self.xc + np.cos(self.teta - 45.0*np.pi/180.0) * self.Rp
            y = self.yc + np.sin(self.teta - 45.0*np.pi/180.0) * self.Rp

            # expands the memory if needed
            self.expandMap(x, y) 

            # sets the memory value to 2 (for obstacle)
            self.setMapValue(x, y, 3.0)

        if self.Lp < 0.40:
            # defines the coordinaes of the obstacle from the distance and angle
            # for the left precise sensor
            x = self.xc + np.cos(self.teta + 45.0*np.pi/180.0) * self.Lp
            y = self.yc + np.sin(self.teta + 45.0*np.pi/180.0) * self.Lp

            # expands the memory if needed
            self.expandMap(x, y) 

            # sets the memory value to 2 (for obstacle)
            self.setMapValue(x, y, 3.0)

    
    def definePot(self):
        """ calculates the potential map using the k-NN approach 
        Currently it sums the central value by 8 neighbours + itself. """

        self.potential = self.map.copy()
        a = self.map[:,2:] + self.map[:,1:-1] + self.map[:,:-2]
        b = a[2:,:] + a[1:-1,:] + a[:-2,:]
        self.potential[1:-1, 1:-1] = b


    def getDirection(self):
        """ defines the direction (in radians relative to the current 
        direction of the robot) based on the memory of the robot about 
        the environment (objects and walls) and past trajectory """

        res = 5
        corr = 0.0

        ix = int( self.xc / self.dx_map)
        iy = int( self.yc / self.dx_map)

        region = self.map[iy-res:iy+res+1, ix-res:ix+res+1].copy()
        if region.shape[0]<=1.0 or region.shape[1]<=1 or np.min(region) >= 1.0:
            return corr

        #region = ut.smooth(region)
        #region = ut.smooth(region)
        #region = ut.smooth(region)
        #region = ut.smooth(region)
        #region = ut.smooth(region)
        #region = ut.smooth(region)
        #dx, dy = ut.getGradient(region, self.dx_map)
        dx, dy = ut.getWeightned(region, self.dx_map)
        d_teta = ut.angle(dx, dy)
        if d_teta != 0.0:
            corr = ut.angleDiff(self.teta, d_teta)*0.5# * ((dx*dx+dy*dy)**0.5) / 0.35
        return corr


class Robot:
    def __init__(self, dtau = 0.1):
        self.dtau = dtau
        self.x = 0.7
        self.y = 0.7
        self.dr = 0.0
        self.dl = 0.0
        self.R  = 0.1
        self.b  = 0.1
        self.tau = 0.0
        self.teta = np.random.random()*pi2 #setting a random initial angle
        self.dd = 0.0
        self.dteta = 0.0
        self.alphs = 0.0
        """adding right and keft sensors"""
        self.objects = None
        self.logic = None

        # Sensors of the robot
        # General sensor which defines the distance to the closest object
        # around the robot 
        self.sensor   = Sensor(self.x, self.y)

        # Angle sensors: direct is a direction of the sensor in relatin 
        # to the direction of the robot itself, angle is sector angle of
        # the sensor
        self.sensorL  = AngleSensor(self.x, self.y, direct= 45., angle=80.)
        self.sensorR  = AngleSensor(self.x, self.y, direct=-45., angle=80.)
        
        # Precise sensors with a narrow angle
        self.sensorD  = AngleSensor(self.x, self.y, direct= 0.0, angle=8.)
        self.sensorLp = AngleSensor(self.x, self.y, direct= 45., angle=8.)
        self.sensorRp = AngleSensor(self.x, self.y, direct=-45., angle=8.)

        # This is needed to collect the data before passing them to 
        # the sesors to speeed up the code.
        self.sensorData = SensorData()

        self.memory = Memory()

    def detectObjects(self, room):
        """ Collects the data from different sensors and stors it in
        the memory of the robot for futher analysis """

        self.sensorData = self.sensor.prepareData(room)
        self.memory.RL = self.sensorL.Distance(self.sensorData)
        self.memory.RR = self.sensorR.Distance(self.sensorData)
        self.memory.RD = self.sensorD.Distance(self.sensorData)
        self.memory.Lp = self.sensorLp.Distance(self.sensorData)
        self.memory.Rp = self.sensorRp.Distance(self.sensorData)
        self.memory.R  = self.sensor.Distance(self.sensorData)
        # Updates memory map
        self.memory.updateMap(self)


    def defineState(self):
        """ Defines the state of the robot depending on the 
        environment. Two states for now: normal and stuck """

        # If the robot is stuck in some closed space, like corners
        if self.memory.RR <= 0.15 and self.memory.RL <= 0.15:
            self.memory.setState(State.stuck)
        
        # Normal operation of the robot
        if self.memory.state == State.stuck:
            if self.memory.RR >= 0.25 or self.memory.RL >= 0.25:
                self.memory.setState(State.normal)
        
                
    def setupLogic(self, logic):
        """ Assignes the logic instance to robot """

        self.logic = logic

    def move(self):
        """ Robot performs a step, updates its coordinates and angles.
        Aligns the sensors as well to ensure that they move and turn with robot """

        self.dl = self.dd - self.dtau * self.b * self.dteta * 0.5 #left wheel
        self.dr = self.dd + self.dtau * self.b * self.dteta * 0.5 #right wheel
        self.x = self.x + self.dtau * self.dd * np.cos(self.teta + self.dtau * self.dteta * 0.5)
        self.y = self.y + self.dtau * self.dd * np.sin(self.teta + self.dtau * self.dteta * 0.5)
        self.teta = self.teta + self.dtau * self.dteta
        
        self.teta = self.teta - pi2 if self.teta >= pi2 else self.teta
        self.teta = self.teta + pi2 if self.teta <  0.0 else self.teta
        
        # Alligns the sensors to the robot itself (self contains the robot information)
        self.sensor.align(self)
        self.sensorL.align(self)
        self.sensorR.align(self)
        self.sensorD.align(self)
        self.sensorLp.align(self)
        self.sensorRp.align(self)

        # Copies information from robot (self) to its memory
        self.memory.copy(self)
        self.memory.addXY(self.x, self.y)
        self.memory.tau += self.dtau
        self.tau += self.dtau
                
    def Analyze(self):
        """ Main logic routine. The logic itself is defines in the main
        simulation by overwriting the setup and calculate methods of the
        Logic class and then assigning it to robot """
        self.dd, self.dteta = self.logic.calculate(self.memory)
