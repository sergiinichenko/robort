# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 09:42:09 2020

@author: nichenko_s
"""
import numpy as np

class SensorData:
    def __init__(self):
        self.x = np.array([])
        self.y = np.array([])
        self.r = np.array([])
        self.r1x = np.array([])
        self.r1y = np.array([])

class Sensor:
    def __init__(self, x = 0.0, y = 0.0):
        self.x = x
        self.y = y
    
    def prepareData(self, room):
        sdata = SensorData()
        x = room.x - self.x
        y = room.y - self.y
        cond = (x < 0.5) & (x > -0.5) & (y < 0.5) & (y > -0.5)
        sdata.x = x[cond]
        sdata.y = y[cond]
        sdata.r = np.sqrt(sdata.x * sdata.x + sdata.y * sdata.y)
        sdata.r1x = sdata.x / sdata.r
        sdata.r1y = sdata.y / sdata.r
        return sdata

    def Distance(self, data):
        """calculation of distance to objects in room"""
        if data.r.shape[0] != 0:
            return data.r.min()
        else:
            return 10.0
    
    def align(self, other):
        self.x = other.x
        self.y = other.y
    

class AngleSensor(Sensor):
    def __init__(self, x=0., y=0., teta=0.0, direct=0., angle=90.):
        self.x = x
        self.y = y
        self.teta = teta
        self.direct = direct
        self.angle = angle

    def align(self, other):
        self.x = other.x
        self.y = other.y
        self.teta = other.teta * 180.0/np.pi
    
    def Distance(self, data):
        a = self.teta + self.direct
        al = a + self.angle * 0.5
        ar = a - self.angle * 0.5
        
        al = 360.0+al if al < 0.0 else al
        ar = 360.0+ar if ar < 0.0 else ar
        al = al-360.0 if al >= 360.0 else al
        ar = ar-360.0 if ar >= 360.0 else ar
        
        val = data.x/data.r
        ang = np.arccos(val)*180.0/np.pi
        ang[data.y<0.0] = 360-ang[data.y<0.0]
        
        cond = False
        if al > ar :  
            cond = (ang < al) & (ang > ar)
        if al < ar :  
            cond = ((ang > 0.) & (ang < al)) | ((ang < 360.) & (ang > ar))
        if cond.any():
            return data.r[cond].min()
        else:
            return 10.0
