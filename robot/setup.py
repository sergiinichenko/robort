import pip

def install_p(package):
    if hasattr(pip, 'main'):
        pip.main(["install", package])
    else:
        pip._internal.main(["install", package])

if __name__ == "__main__":
    install_p("numpy")
    install_p("matplotlib")
    install_p("scipy")
    